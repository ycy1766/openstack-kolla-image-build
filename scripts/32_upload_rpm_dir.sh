#!/bin/bash
export repo_name="updates"
export nexus3_admin_password="openstack_test"
export rpm_file_path="/root/pcs_repo"

ls ~/pcs_repo | awk -v repo_name="$repo_name" -v nexus3_admin_password="$nexus3_admin_password" -v rpm_file_path="$rpm_file_path" \
'{print " \
curl -v \
--user admin:"nexus3_admin_password \
" --insecure \
--upload-file " rpm_file_path"/"$1 \
" http://localhost:8081/repository/CLOUD_REPOs/"repo_name"/"$1
}'|sh 
