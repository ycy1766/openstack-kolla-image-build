#!/bin/bash

echo "Install packages for ansible"
yum install -y python-devel libffi-devel gcc openssl-devel libselinux-python epel-release

echo "Install pip and ansible"
yum install -y python-pip
pip install -U pip==9.0.3 

echo "Install ansible-2.7.1"
pip install ansible==2.7.1

echo "Install tox"
pip install -U toc

echo "Install vi ->vim "
yum install -y vim-enhanced.x86_64
echo "alias vi='vim'" >> ~/.bashrc
. ~/.bashrc
