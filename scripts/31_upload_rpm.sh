#!/bin/bash

repo_name="pcs_repo"
nexus3_admin_password="openstack_test"
rpm_file_path="/root"
rpm_file_name="zabbix-agent-4.0.0-2.el7.x86_64.rpm"


curl -v \
--user admin:${nexus3_admin_password}  \
--insecure \
--upload-file ${rpm_file_path}/${rpm_file_name} \
http://localhost:8081/repository/CLOUD_REPOs/${repo_name}/${rpm_file_name}
