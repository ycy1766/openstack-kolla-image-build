
해당 ansible playbook은 kolla build 하여 `nexus3` 저장소로 자동화 하기 위한 스크립트 입니다.
python관련 패키지는 `virtualenv`로 설치/실행 됩니다.

## 과정 

1. `docker` 및 `kolla` 등을 빌드 하기 위한 환경 설정
  
2. `docker-ce` 설치 / 설정
  

3. `virtualenv` 설치 / 설정

  
4. `nexus3` 설치 / 설정



5. `kolla` 빌드

  
빌드 할때 profile설정(ex. aux,main,default ...) 으로 할 경우 `use_kolla_build_profile` 변수를 yes로 설정 한뒤 profile정보를 `kolla_build_images` 변수로 지정 하여 실행 합니다.

profile 설정 없이 nova, cinder 등 과 같은 프로젝트 이름으로 빌드 할 경우 `use_kolla_build_profile` 변수를 no로 설정 한뒤 프로젝트이름을 `kolla_build_images`  변수로 지정 하여 실행 합니다.