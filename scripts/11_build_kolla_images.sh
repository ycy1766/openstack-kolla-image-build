#!/bin/bash
VENV_BIN=`cat ./inventory/group_vars/all.yml | grep venv_directory | awk -F\" '{print $2}'`

$VENV_BIN/bin/ansible-playbook playbooks/11_build_kolla_images.yml 
