#!/bin/bash

VENV_BIN=`cat ./inventory/group_vars/all.yml | grep venv_directory | awk -F\" '{print $2}'`

$VENV_BIN/bin/ansible-playbook playbooks/02_create_local_nuxus_repo.yml 
